﻿using System.Collections.Generic;
using System.Linq;
using SquadDailyReport.Models;

namespace SquadDailyReport.Extensions
{
    public static class ListExtensions
    {
        public static bool IsPendenciesResolved(this List<Pendencia> pendencias)
        {
            return pendencias
                .Where(x => !string.IsNullOrEmpty(x.Descricao))
                .All(x => x.Resolvido);
        }

        public static bool IsImpedimentsResolved(this List<Impedimento> impedimentos)
        {
            return impedimentos
                .All(x => string.IsNullOrEmpty(x.Estoria));
        }
    }
}