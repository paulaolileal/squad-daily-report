﻿using System;

namespace SquadDailyReport.Models
{
    public class Impedimento
    {
        public string Estoria { get; set; }
        public string Motivo { get; set; }
        public string Data { get; set; }

        public Impedimento()
        {
            Data = DateTime.Now.ToString("dd/MM/yyyy");
            Estoria = Motivo = string.Empty;
        }
    }
}