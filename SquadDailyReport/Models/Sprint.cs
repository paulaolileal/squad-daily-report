﻿using System;

namespace SquadDailyReport.Models
{
    public class Sprint
    {
        public int Atual { get; set; }
        public string Inicio { get; set; }
        public string Fim { get; set; }
        public int Estoque { get; set; }
        public int Homologacao { get; set; }

        public Sprint()
        {
            Inicio = DateTime.Now.ToString("dd/MM/yyyy");
            Inicio = DateTime.Now.AddDays(14).ToString("dd/MM/yyyy");
        }
    }
}