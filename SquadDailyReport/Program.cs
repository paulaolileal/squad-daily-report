﻿using System;
using System.IO;
using McMaster.Extensions.CommandLineUtils;
using Microsoft.Extensions.DependencyInjection;
using SquadDailyReport.Commands;

namespace SquadDailyReport
{
    [Command(UnrecognizedArgumentHandling = UnrecognizedArgumentHandling.StopParsingAndCollect)]
    [Subcommand(
        typeof(GenerateCommand),
        typeof(SettingsCommand)
    )]
    [HelpOption()]
    public class Program
    {
        private void OnExecute(CommandLineApplication app)
        {
            app.ShowHelp();
        }

        public static int Main(string[] args)
        {
            try
            {
                var path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "DailyReports");
                if (!Directory.Exists(path)) Directory.CreateDirectory(path);

                var services = new ServiceCollection()
                    .BuildServiceProvider();

                var app = new CommandLineApplication<Program>();
                app.Conventions.UseDefaultConventions();
                app.Conventions.UseConstructorInjection(services);

                return app.Execute(args);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error\t{0}", e.Message);
            }

            return -1;
        }
    }
}